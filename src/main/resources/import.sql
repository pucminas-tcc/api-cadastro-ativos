INSERT INTO sica.tipo_equipamento (id, marca, modelo, serie, ano, detalhe) VALUES (1, 'LIEBHERR', 'R984C', '7388027', 2000, 'Escavadeira Hidráulica LIEBHERR R984C');
INSERT INTO sica.tipo_equipamento (id, marca, modelo, serie, ano, detalhe) VALUES (2, 'KOMATSU', 'SAA6D125E', 0, 0, 'Motor Diesel PC400');

INSERT INTO sica.equipamento (id, nome, horimetro, tipo_equipamento_id) VALUES (1, 'Escavadeira', 1116.30, 1);
INSERT INTO sica.equipamento (id, nome, horimetro, tipo_equipamento_id) VALUES (2, 'Motor', 0, 2);