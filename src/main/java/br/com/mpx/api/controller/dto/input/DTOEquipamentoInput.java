package br.com.mpx.api.controller.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOEquipamentoInput {
	
	private String nome;
	private Double horimetro;
	private DTOTipoEquipamentoInput tipoEquipamento;

}
