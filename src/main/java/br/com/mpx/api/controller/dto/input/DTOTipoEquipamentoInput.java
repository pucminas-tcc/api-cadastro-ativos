package br.com.mpx.api.controller.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOTipoEquipamentoInput {
	
	private String detalhe;
	private String marca;
	private String modelo;
	private String serie;

}
