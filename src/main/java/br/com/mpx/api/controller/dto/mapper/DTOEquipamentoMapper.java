package br.com.mpx.api.controller.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mpx.api.controller.dto.DTOEquipamento;
import br.com.mpx.api.controller.dto.input.DTOEquipamentoInput;
import br.com.mpx.domain.model.Equipamento;

@Component
public class DTOEquipamentoMapper {
	@Autowired
	private ModelMapper modelMapper;

	public Equipamento dtoToEntidade(DTOEquipamento dtoEquipamento) {
		return modelMapper.map(dtoEquipamento, Equipamento.class);
	}

	public DTOEquipamento entidadeToDto(Equipamento Equipamento) {
		return modelMapper.map(Equipamento, DTOEquipamento.class);
	}

	public List<DTOEquipamento> listaEntidadeToListaDto(List<Equipamento> Equipamentos) {
		return Equipamentos.stream().map(Equipamento -> entidadeToDto(Equipamento)).collect(Collectors.toList());
	}

	public List<Equipamento> listaDtoToListaEntidade(List<DTOEquipamento> dtoEquipamentos) {
		return dtoEquipamentos.stream().map(dto -> this.dtoToEntidade(dto)).collect(Collectors.toList());
	}

	public void copiarToEntidade(DTOEquipamentoInput EquipamentoInput, Equipamento Equipamento) {
		modelMapper.map(EquipamentoInput, Equipamento);
	}

}
