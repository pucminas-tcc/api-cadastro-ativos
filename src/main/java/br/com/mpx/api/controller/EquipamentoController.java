package br.com.mpx.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mpx.api.controller.dto.DTOEquipamento;
import br.com.mpx.api.controller.dto.input.DTOEquipamentoInput;
import br.com.mpx.api.controller.dto.mapper.DTOEquipamentoInputMapper;
import br.com.mpx.api.controller.dto.mapper.DTOEquipamentoMapper;
import br.com.mpx.api.controller.openapi.EquipamentoControllerOpenApi;
import br.com.mpx.domain.exception.NegocioException;
import br.com.mpx.domain.model.Equipamento;
import br.com.mpx.domain.service.EquipamentoService;

@RestController
@RequestMapping("/ativos/equipamentos")
public class EquipamentoController implements EquipamentoControllerOpenApi {

	@Autowired
	private DTOEquipamentoMapper equipamentoDTOMapper;

	@Autowired
	private DTOEquipamentoInputMapper equipamentoInputDTOMapper;

	@Autowired
	private EquipamentoService service;
	
	@GetMapping
	public List<DTOEquipamento> listar() {
		List<Equipamento> equipamentos = service.listar();

		return equipamentoDTOMapper.listaEntidadeToListaDto(equipamentos);
	}

	@GetMapping("/{EquipamentoId}")
	public DTOEquipamento buscar(@PathVariable Long EquipamentoId) {
		Equipamento equipamento = service.buscarOuFalhar(EquipamentoId);

		return equipamentoDTOMapper.entidadeToDto(equipamento);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public DTOEquipamento adicionar(@RequestBody DTOEquipamentoInput equipamentoInput) {
		try {
			Equipamento equipamento = equipamentoInputDTOMapper.dtoToEntidade(equipamentoInput);

			equipamento = service.salvar(equipamento);

			return equipamentoDTOMapper.entidadeToDto(equipamento);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@PutMapping("/{EquipamentoId}")
	public DTOEquipamento atualizar(@PathVariable Long EquipamentoId,
			@RequestBody DTOEquipamentoInput equipamentoInput) {

		Equipamento equipamentoAtual = service.buscarOuFalhar(EquipamentoId);

		equipamentoDTOMapper.copiarToEntidade(equipamentoInput, equipamentoAtual);

		equipamentoAtual = service.salvar(equipamentoAtual);

		return equipamentoDTOMapper.entidadeToDto(equipamentoAtual);

	}

	@DeleteMapping("/{EquipamentoId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long EquipamentoId) {
		service.excluir(EquipamentoId);
	}

}
