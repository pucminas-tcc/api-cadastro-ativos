package br.com.mpx.api.controller.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;

import br.com.mpx.domain.exception.EquipamentoNaoEncontradoException;
import lombok.extern.slf4j.Slf4j;

/**
 * Classe centralizadora de tratamento de controller exceptions.</br></br>
 * 
 * Extende {@link ResponseEntityExceptionHandler} que é uma conveniente classe base para 
 * {@link ControllerAdvice @ControllerAdvice} que desejam fornecer tratamento de exceção
 * centralizado em todos os métodos {@code @RequestMapping} através dos métodos {@code @ExceptionHandler}.</br></br>
 * 
 * Os detalhes do response body da api segue o padrão detalhado na 
 * <a href="https://tools.ietf.org/html/rfc7807">RFC 7807</a></br></br>
 * 
 * @author Alex Pereira Maranhão
 *
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	public static final String MSG_ERRO_GENERICA_USUARIO_FINAL = "Ocorreu um erro interno. "
			+ "Verifique os parâmetros de entrada ou tente novamente mais tarde.";
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Customiza a resposta para HttpMediaTypeNotAcceptableException.
	 * <p>Esse método delega para {@link #handleExceptionInternal}.
	 * @param ex a exceção
	 * @param headers os cabeçalhos que serão escritos na response
	 * @param status o response status selecionado
	 * @param request a request atual
	 * @return uma instância de {@code ResponseEntity}
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Problema problema = Problema.builder()
		.timestamp(OffsetDateTime.now())		
		.titulo("media-type não suportado.")
		.detalhe("O media-type " + request.getHeader("content-type") + " não é suportado")
		.status(status.value())
		.build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		
		return handleValidationInternal(ex, headers, status, request, ex.getBindingResult());
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleUncaught(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;		
		TipoProblema tipoProblema = TipoProblema.ERRO_DE_SISTEMA;
		String detalhe = MSG_ERRO_GENERICA_USUARIO_FINAL;

		log.error(ex.getMessage(), ex);
		
		Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(detalhe)
				.build();

		return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<?> handleEntidadeNaoEncontrada(AccessDeniedException ex, WebRequest request) {

		HttpStatus status = HttpStatus.FORBIDDEN;
		TipoProblema tipoProblema = TipoProblema.ACESSO_NEGADO;
		String detalhe = ex.getMessage();

		Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe)
				.titulo(detalhe)
				.mensagemUsuario("Você não possui permissão para executar essa operação.")
				.build();

		return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
	}
	
	@ExceptionHandler(EquipamentoNaoEncontradoException.class)
	public ResponseEntity<?> handleEntidadeNaoEncontrada(EquipamentoNaoEncontradoException ex,
			WebRequest request) {
		
		HttpStatus status = HttpStatus.NOT_FOUND;
		TipoProblema tipoProblema = TipoProblema.RECURSO_NAO_ENCONTRADO;
		String detalhe = ex.getMessage();
		
		Problema problem = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(detalhe)
				.build();
		
		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	/**
	 * Customiza a resposta para NoHandlerFoundException.
	 * <p>
	 * Esse método delega para {@link #handleExceptionInternal}.
	 * 
	 * @param ex      a exception
	 * @param headers os headers para ser escrito no response
	 * @param status  o response status selecionado
	 * @param request a current request
	 * @return uma instância de {@code ResponseEntity}
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		TipoProblema tipoProblema = TipoProblema.RECURSO_NAO_ENCONTRADO;
		String detalhe = String.format("O recurso %s, é inexistente.", ex.getRequestURL());

		Problema problema = this.criarProblemaBuilder(status, tipoProblema, detalhe).build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			org.springframework.web.bind.MissingServletRequestParameterException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
	    TipoProblema tipoProblema = TipoProblema.PARAMETRO_INVALIDO;
	    String detalhe = "O parâmetro " + ex.getParameterName() + " do tipo " + ex.getParameterType() + "  não foi informado.";
	    
	    Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe).build();
	    
		return super.handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		if(ex instanceof MethodArgumentTypeMismatchException) {
			return handleMethodArgumentTypeMismatch(
					(MethodArgumentTypeMismatchException) ex, headers, status, request);
		}
		
		return super.handleTypeMismatch(ex, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		
		if(rootCause instanceof InvalidFormatException) {
			return handleInvalidFormat((InvalidFormatException) rootCause, headers, status, request);
		} else if (rootCause instanceof PropertyBindingException) {
			return handlePropertyBinding((PropertyBindingException) rootCause, headers, status, request);
		}
		
		TipoProblema tipoProblema = TipoProblema.MENSAGEM_INCOMPREENSIVEL;
		String detalhe = "Existe algum erro no corpo da requisição.";
		
		Problema problem = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL)
				.build();
		
		return handleExceptionInternal(ex, problem, headers, status, request);
	}

	/**
	 * Customiza a resposta para MethodArgumentNotValidException.
	 * <p>ESse método delega para {@link #handleExceptionInternal}.
	 * @param ex a exceção
	 * @param headers os cabeçalhos para serem escritos na response
	 * @param status o response status selecionado
	 * @param request a request atual
	 * @return uma instância de {@code ResponseEntity}
	 */	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		return handleValidationInternal(ex, headers, status, request, ex.getBindingResult());
	}

	/**
	 * Local único para personalizar o corpo da resposta de todos os tipos de
	 * exceção.
	 * <p>
	 * A implementação padrão define o atritubo de request
	 * {@link WebUtils#ERROR_EXCEPTION_ATTRIBUTE} e cria um {@link ResponseEntity}
	 * de um body, headers, e status.
	 * 
	 * @param ex      a exception
	 * @param body    o body para a resposta
	 * @param headers os headers para a resposta
	 * @param status  o response status
	 * @param request a current request
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		if (body == null) {
			body = Problema.builder()
					.timestamp(OffsetDateTime.now())
					.titulo(status.getReasonPhrase())
					.status(status.value())
					.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL).build();
		} else if (body instanceof String) {
			body = Problema.builder()
					.timestamp(OffsetDateTime.now())
					.titulo(String.valueOf(body))
					.status(status.value())
					.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL).build();
		}

		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	private Problema.ProblemaBuilder criarProblemaBuilder(HttpStatus status, TipoProblema tipoProblema, String detalhe){
		
		return Problema.builder()
				.timestamp(OffsetDateTime.now())
				.status(status.value())
				.titulo(tipoProblema.getTitulo())
				.detalhe(detalhe);
		
	}
	
	private ResponseEntity<Object> handleValidationInternal(Exception ex, HttpHeaders headers, HttpStatus status,
			WebRequest request, BindingResult bindingResult){
		
		TipoProblema tipoProblema = TipoProblema.DADOS_INVALIDOS;
		
		String detalhe = "Campos inválidos. Por favor, faça o preenchimento correto e tente novamente.";
		
		List<Problema.Campo> camposComProblema = bindingResult.getAllErrors().stream()
				.map(campoComErro -> {
					String mensagem = messageSource.getMessage(campoComErro, LocaleContextHolder.getLocale());
					String nome = campoComErro.getObjectName();
					
					if(campoComErro instanceof FieldError) {
						nome = ((FieldError) campoComErro).getField();
					}
					
					return Problema.Campo.builder()
							.nome(nome)
							.mensagemUsuario(mensagem)
							.build();
				}).collect(Collectors.toList());
		
		Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(detalhe)
				.campos(camposComProblema)
				.build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	private ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request){
		TipoProblema tipoProblema = TipoProblema.PARAMETRO_INVALIDO;
		
		String detalhe = String.format("O parâmetro de URL '%s' recebeu o valor inválido '%s', "
				+ ". Por favor, informe um valor do tipo %s.",
				ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());
		
		Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL)
				.build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	private ResponseEntity<Object> handleInvalidFormat(InvalidFormatException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String caminho = juntarCaminhoPropriedadeJson(ex.getPath());
		
		TipoProblema tipoProblema = TipoProblema.MENSAGEM_INCOMPREENSIVEL;
		String detalhe = String.format("A propriedade '%s' recebeu o valor inválido '%s'. "
				+ " Por favor, informe um valor do tipo %s.",
				caminho, ex.getValue(), ex.getTargetType().getSimpleName());
		
		Problema problem = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL)
				.build();
		
		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	private ResponseEntity<Object> handlePropertyBinding(PropertyBindingException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		String caminho = juntarCaminhoPropriedadeJson(ex.getPath());
		
		TipoProblema tipoProblema = TipoProblema.MENSAGEM_INCOMPREENSIVEL;
		String detalhe = String.format("A propriedade '%s' não existe. ", caminho);

		Problema problema = criarProblemaBuilder(status, tipoProblema, detalhe)
				.mensagemUsuario(MSG_ERRO_GENERICA_USUARIO_FINAL)
				.build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	private String juntarCaminhoPropriedadeJson(List<Reference> references) {
		return references.stream()
			.map(ref -> ref.getFieldName())
			.collect(Collectors.joining("."));
	}
}