package br.com.mpx.api.controller.exceptionhandler;

import lombok.Getter;

@Getter
public enum TipoProblema {

	ERRO_DE_SISTEMA("Erro de sistema"),
	PARAMETRO_INVALIDO("Parâmetro inválido"),
	MENSAGEM_INCOMPREENSIVEL("Mensagem incompreensível"),
	RECURSO_NAO_ENCONTRADO("Recurso não encontrado"),
	ENTIDADE_EM_USO("Entidade em uso"),
	ERRO_NEGOCIO("Violação de regra de negócio"),
	ACESSO_NEGADO("Acesso negado"),
	DADOS_INVALIDOS("Dados inválidos");
	
	private String titulo;
	
	TipoProblema(String titulo) {	
		this.titulo = titulo;
	}
	
}