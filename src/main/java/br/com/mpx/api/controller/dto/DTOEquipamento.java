package br.com.mpx.api.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOEquipamento {

	private String nome;
	private Double horimetro;
	private DTOTipoEquipamento tipoEquipamento;
	
}
