package br.com.mpx.api.controller.openapi;

import java.util.List;

import br.com.mpx.api.controller.dto.DTOEquipamento;
import br.com.mpx.api.controller.dto.input.DTOEquipamentoInput;
import br.com.mpx.api.controller.exceptionhandler.Problema;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Api(tags = "Equipamentos")
public interface EquipamentoControllerOpenApi {

	@ApiOperation("Lista as equipamentos")
	List<DTOEquipamento> listar();
	
	@ApiOperation("Busca um equipamento por ID")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID do equipamento inválido", response = Problema.class),
		@ApiResponse(code = 404, message = "Equipamento não encontrada", response = Problema.class)
	})
	DTOEquipamento buscar(
			@ApiParam(value = "ID de um equipamento", example = "1", required = true)
			Long equipamentoId);
	
	@ApiOperation("Cadastra um equipamento")
	@ApiResponses({
		@ApiResponse(code = 201, message = "Equipamento cadastrado"),
	})
	DTOEquipamento adicionar(
			@ApiParam(name = "corpo", value = "Representação de uma novo equipamento", required = true)
			DTOEquipamentoInput equipamentoInput);
	
	@ApiOperation("Atualiza um equipamento por ID")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Equipamento atualizado"),
		@ApiResponse(code = 404, message = "Equipamento não encontrado", response = Problema.class)
	})
	DTOEquipamento atualizar(
			@ApiParam(value = "ID de um equipamento", example = "1", required = true) 
			Long equipamentoId,
			
			@ApiParam(name = "corpo", value = "Representação de um equipamento com os novos dados", required = true)
			DTOEquipamentoInput equipamentoInput);
	
	@ApiOperation("Exclui um equipamento por ID")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Equipamento excluído"),
		@ApiResponse(code = 404, message = "Equipamento não encontrado", response = Problema.class)
	})
	void remover(
			@ApiParam(value = "ID de um equipamento", example = "1", required = true)
			Long equipamentoId);
	
}


