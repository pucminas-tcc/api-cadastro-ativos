package br.com.mpx.domain.exception;

public class EquipamentoNaoEncontradoException extends NegocioException {

	private static final long serialVersionUID = 1L;

	public EquipamentoNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);		
	}

	public EquipamentoNaoEncontradoException(String message) {
		super(message);
	}
	
	public EquipamentoNaoEncontradoException(Long id) {
		super("Equipamento de ID " + id + " não encontrado.");
	}

}
