package br.com.mpx.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mpx.domain.model.Equipamento;

public interface EquipamentoRepository extends JpaRepository<Equipamento, Long> {

}
