package br.com.mpx.domain.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mpx.domain.exception.EquipamentoNaoEncontradoException;
import br.com.mpx.domain.model.Equipamento;
import br.com.mpx.domain.repository.EquipamentoRepository;

@Service
public class EquipamentoService {
	@Autowired
	private EquipamentoRepository equipamentoRepository;
	
	@Transactional
	public Equipamento salvar(Equipamento equipamento) {

		return equipamentoRepository.save(equipamento);
		
	}
	
	public Equipamento buscarOuFalhar(Long equipamentoId) {
		return equipamentoRepository.findById(equipamentoId)
			.orElseThrow(() -> new EquipamentoNaoEncontradoException(equipamentoId));
	}

	public List<Equipamento> listar() {
		List<Equipamento> equipamentos = equipamentoRepository.findAll();
		
		return equipamentos;
	}
	
	@Transactional
	public void excluir(Long id) {
		equipamentoRepository.deleteById(id);
	}

}
