package br.com.mpx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class CadastroAtivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroAtivosApplication.class, args);
	}

}
