FROM openjdk:8-jdk-alpine

ARG ambiente
ENV ambiente=${ambiente:-homolog} 

RUN echo $ambiente

ARG SPRING_PROFILES_ACTIVE
ENV SPRING_PROFILES_ACTIVE=$ambiente 

ADD target/eco-faturamento-web-service.jar /opt/api-cadastro-ativos.jar

ENTRYPOINT ["java", "-jar", "/opt/api-cadastro-ativos.jar", "--spring.profiles.active=${SPRING_PROFILES_ACTIVE}"]