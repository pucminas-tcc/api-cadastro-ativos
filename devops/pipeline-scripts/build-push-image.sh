#!/bin/bash

REGISTRY=docker-registry.default.svc.cluster.local:5000

docker build --build-arg ambiente=$ambiente -f devops/dockerfiles/api-cadastro-ativos.dockerfile -t api-cadastro-ativos:$CI_COMMIT_REF_NAME .

docker tag api-cadastro-ativos:$CI_COMMIT_REF_NAME $REGISTRY/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}:$CI_COMMIT_REF_NAME

docker login -u openshift-registry -p $OPENSHIFT_DEVOPS_TOKEN $REGISTRY 

docker --debug push $REGISTRY/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}:$CI_COMMIT_REF_NAME