#!/bin/bash

yum install gettext -y --disableplugin=fastestmirror

oc login $OPENSHIFT --token=$OPENSHIFT_DEVOPS_TOKEN --insecure-skip-tls-verify=true

oc project api-cadastro-ativos

mkdir devops/kubernetes-env

commit=${commit:0:8}

for filename in devops/kubernetes/*.yml; do

    name=${filename##*/}
    envsubst < devops/kubernetes/$name > devops/kubernetes-env/$name

done

oc apply -f devops/kubernetes-env

# configura o acesso externo para os balancers cloudprod
if [ "$acesso" == "externo" ]; then
    oc label route/${CI_PROJECT_NAME}-${ambiente} route=externo --overwrite
else
    oc label route/${CI_PROJECT_NAME}-${ambiente} route=interno --overwrite
fi

