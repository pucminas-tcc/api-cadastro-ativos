#!/bin/bash

# compila via maven
MAVEN_CLI_OPTS="-s devops/resources/settings.xml --batch-mode --errors --fail-at-end --show-version -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
mvn $MAVEN_CLI_OPTS clean package